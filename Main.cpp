#include <iostream>
#include <ctime>

const int N = 7;
int sum;

int main()
{
	std::time_t t = std::time(NULL);
	std::tm* now = std::localtime(&t);
	int array[N][N];

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << " ";
		}
		std::cout << "\n";
		if (i == now->tm_mday % N)
		{
			for (int j = 0; j < N; j++)
			{
				sum += array[i][j];
			}
		}

	}

	std::cout << sum << "\n";

	return 0;
}